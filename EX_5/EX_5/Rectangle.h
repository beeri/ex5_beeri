#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{

	private:
		double _length;
		double _width;

	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name)
			:Polygon(type, name)
		{
			if (width == 0 || length == 0)
			{
				std::cerr << "error width/length are 0 " << "\n";
				_exit(1);
			}
			_length = length;
			_width = width;

			_points.push_back(a);
			
			Point b(a.getX() + length, a.getY() + width);
			_points.push_back(b);
		}


		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);
		virtual double getArea() const;
		double getPerimeter() const;

		// override functions if need (virtual + pure virtual)

	};
}