#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name)
		: Polygon(type, name)
	{
		
		if ((a.getX() == b.getX() && a.getX() == c.getX()) || (a.getY() == b.getY() && a.getY() == c.getY()))
		{
			std::cerr << "error, this will not be a triangle" << "\n";
			_exit(1);
		}
		_points.push_back(a);
		_points.push_back(b);
		_points.push_back(c);
	}
	
	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);
	virtual double getArea() const;
	virtual double getPerimeter() const;
	
	// override functions if need (virtual + pure virtual)
};