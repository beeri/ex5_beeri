#include "Point.h"

#include <math.h>

//getters
double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

//c-tor
Point::Point(double x, double y)
{
	_y = y;
	_x = x;
}

//copy c-tor
Point::Point(const Point& other)
{
	
	_y = other.getY();
	_x = other.getX();
}

  // add new point x+y to the old points
Point Point::operator+(const Point& other) const
{
	Point newPoint(_x + other.getX(), _y + other.getY());

	return newPoint;
}

 
Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();

	return *this;
}

//distance between two points
double Point::distance(const Point& other) const
{
	double ret = pow(_x - other.getX(), 2) + pow(_y - other.getY(), 2);
	ret = sqrt(ret);
	
	return ret;
}
