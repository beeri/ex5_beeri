#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name)
		: Shape(type, name)
	{
		_points.push_back(a);
		_points.push_back(b);
	}


	virtual void move(const Point& other);
	virtual double getPerimeter() const ;
	virtual double getArea() const ;
	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	// override functions if need (virtual + pure virtual)

private:
	std::vector<Point> _points;
};