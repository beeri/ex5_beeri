#include "Circle.h"


void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

//getters
const Point& Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}


// the area of the circel
double Circle::getArea() const
{
	return pow(_radius, 2) * PI;
}

//the perimeter of the circel
double Circle::getPerimeter() const
{
	return (PI * _radius) * 2;
}

//move the circle
void Circle::move(const Point& other)
{
	_center += other;
}