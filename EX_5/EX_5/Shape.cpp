#include "Shape.h"



//getters
std::string Shape::getType() const
{
	return _type;
}

std::string Shape::getName() const
{
	return _name;
}



void Shape::printDetails() const
{
	std::cout << _type.c_str() << "\n";
	std::cout << _name.c_str() << "\n";
	std::cout << getArea() << "\n";
	std::cout << getPerimeter() << "\n";
}


