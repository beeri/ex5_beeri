#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>



class Menu
{
public:


	int ShapesMenu();

	void addArrow(std::vector<Shape> shapes);
	void addCircel(std::vector<Shape> shapes);
	void addTriangle(std::vector<Shape> shapes);
	void addRectangle(std::vector<Shape> shapes);

	void add_shape(std::vector<Shape> shapes);


	// more functions..

private: 
	Canvas _canvas;
};

