#include "Triangle.h"



void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}



// the area of the triangle  -   S =   x1(y2 - y3) + x2(y3 - y1) + x3(y1 - y2) 
double Triangle::getArea() const
{	                      
	double area = 0;

	area += _points[0].getX() * ( _points[2].getY() - _points[1].getY() );
	area += _points[1].getX() * ( _points[0].getY() - _points[2].getY() );
	area += _points[2].getX() * ( _points[1].getY() - _points[0].getY() );

	area = area / 2;

	return area;
}

// the perimeter of the Triangle
double Triangle::getPerimeter() const
{
	double perimeter;
	perimeter += _points[0].distance(_points[1]);
	perimeter += _points[1].distance(_points[2]);
	perimeter += _points[2].distance(_points[0]);

	return perimeter;
}