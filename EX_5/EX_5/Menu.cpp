#include "Menu.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Circle.h"
#include <iostream>

using std::string;
using std::vector;



void Menu::add_shape(vector<Shape> shapes)
{
	auto shape = ShapesMenu();

	switch (shape)
	{
		case 1:
			addArrow(shapes);
		break;
		
		case 2:
			addCircel(shapes);
		break;

		case 3:
			addTriangle(shapes);
			break;

		case 4:
			addRectangle(shapes);
			break;
	}
}

int Menu::ShapesMenu()
{
	int choice;
	do
	{
		std::cout << "Enter 1 - add arrow" << "\n";
		std::cout << "Enter 2 - add circle" << "\n";
		std::cout << "Enter 3 - add triangle" << "\n";
		std::cout << "Enter 4 - add rectangle" << "\n";
		
		std::cin >> choice;
	
	} while (choice < 1 || choice > 4);
	
	return choice;
}

void Menu::addArrow(vector<Shape> shapes)
{
	string name;
	vector<Point> points;
	double x;
	double y;

	for (int i = 1; i < 3; i++) 
	{
		
		std::cout << "Enter the X of point number: " << i << "\n";
		std::cin >> x;
		
		std::cout << "Enter the Y of point number: " << i << "\n";
		std::cin >> y;
		
		points.push_back(Point(x, y));
	}


	std::cout << "Enter the name of the shape: " << "\n";
	std::cin >> name;


	Arrow* a = new Arrow(points[0], points[1], "Arrow", name);
	shapes.push_back(*a);

	a->draw(_canvas);
}


void Menu::addCircel(vector<Shape> shapes)
{
	string name;
	double x;
	double y;
	double radius;

	std::cout << "Enter the X of the center" << "\n";
	std::cin >> x;

	std::cout << "Enter the Y of the center" << "\n";
	std::cin >> y;

	Point center(x,y);

	std::cout << "Enter the radius of the circel" << "\n";
	std::cin >> radius;

	std::cout << "Enter the name of the shape: " << "\n";
	std::cin >> name;


	Circle* a = new Circle(center, radius, "Circel", name);
	shapes.push_back(*a);

	a->draw(_canvas);
}

void Menu::addTriangle(std::vector<Shape> shapes)
{
	string name;
	vector<Point> points;
	double x;
	double y;

	for (int i = 0; i < 4; i++)
	{

		std::cout << "Enter the X of point number: " << i << "\n";
		std::cin >> x;

		std::cout << "Enter the Y of point number: " << i << "\n";
		std::cin >> y;

		points.push_back(Point(x, y));
	}


	std::cout << "Enter the name of the shape: " << "\n";
	std::cin >> name;


	Triangle* a = new Triangle(points[0], points[1],points[2], "Triangle", name);
	shapes.push_back(*a);

	a->draw(_canvas);
}


void Menu::addRectangle(std::vector<Shape> shapes)
{
	string name;
	double x;
	double y;
	double length;
	double width;

	std::cout << "Enter the X of the point" << "\n";
	std::cin >> x;

	std::cout << "Enter the Y of the point" << "\n";
	std::cin >> y;

	Point point(x, y);

	std::cout << "Enter the length of the Rectangle" << "\n";
	std::cin >> length;

	std::cout << "Enter the width of the Rectangle" << "\n";
	std::cin >> width;

	std::cout << "Enter the name of the shape: " << "\n";
	std::cin >> name;


	myShapes::Rectangle* a = new myShapes::Rectangle(point, length, width, "Rectangle", name);
	shapes.push_back(*a);

	a->draw(_canvas);
}