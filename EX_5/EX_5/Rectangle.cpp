#include "Rectangle.h"



void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


double myShapes::Rectangle::getArea() const
{	
	double area = _length * _width;
	
	return area;
}


double myShapes::Rectangle::getPerimeter() const
{
	double perimeter = (_length * 2) + (_width * 2);

	return perimeter;
}



