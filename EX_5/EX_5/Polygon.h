#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	Polygon(const std::string& type, const std::string& name)
		: Shape(name, type)
	{

	}

	virtual void move(const Point& other);
	virtual double getPerimeter() const;
	virtual double getArea() const;
	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);
	// override functions if need (virtual + pure virtual)

protected:
	std::vector<Point> _points;
};