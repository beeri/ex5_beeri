#include "Arrow.h"


void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

double Arrow::getPerimeter() const
{
	return _points[0].distance(_points[1]);
}

double Arrow::getArea() const
{
	return 0;
}


void Arrow::move(const Point& other)
{
	Point newPoint(other.getX(), -(other.getY()) );

	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += newPoint;
	}
}

