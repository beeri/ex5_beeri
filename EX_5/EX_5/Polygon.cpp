#include "Polygon.h"

//add to every point in the vector the 'other' point 
void Polygon::move(const Point& other)
{
	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += other;
	}
}